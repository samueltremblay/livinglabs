<?php

namespace OCA\LivingLabs\AppInfo;

use OCP\AppFramework\App;
use OCP\Util;

class Application extends App {

    public function __construct() {
        parent::__construct('livinglabs');
    }

    public function register() {
        Util::addScript('livinglabs', 'terms_of_service');
        Util::addScript('livinglabs', 'chrome_popup'); 
        Util::addScript('livinglabs', 'public_links');
        Util::addScript('livinglabs', 'smaller_scripts');
        
        // WCAG modifications
        Util::addScript('livinglabs', 'theming');
        Util::addStyle('livinglabs', 'theming');
        Util::addStyle('livinglabs', 'login');
    }
}