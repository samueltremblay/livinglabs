const terms =`
<div lang="en" class="enconst">
<h1 tabindex="1">AAFC Disclaimer</h1> 

Living Laboratories Cloud Data Storage Platform Terms of Use
<br>
Access to the Living Laboratories Cloud Data Storage Platform (the Platform) is restricted to authorized users only. Although the Platform is managed and monitored by Agriculture and Agri-Food Canada (AAFC), the data/information on the Platform is from sources internal and external to the Government of Canada. <br>
<br>
<h2>Usage</h2>
<ul>
    <li>You are an authorized user of the Platform.</li> 
    <li>The Platform may only be used to share data/information (soil quality, hydrographs, greenhouse gas emissions, biodiversity, etc.) that pertain to approved Living Laboratories initiatives and to consult the data/information submitted by other users.</li>
    <li>AAFC gives no warranty, express or implied, and accepts no legal liability or responsibility for the accuracy, completeness or usefulness of any information on the Platform.</li>
    <li>The Platform may only contain information up to the level of security Protected A and you are responsible to ensure that no data above that level is uploaded or saved to the Platform. Protected A information is information that, if compromised, could cause injury to an individual, organization or government. Examples include: contracts, tenders, unsolicited proposals, and pleadings. </li>
    <li>The Platform may not be used to upload or save personal information as defined under the <a href='http://laws-lois.justice.gc.ca/eng/acts/P-21/' class='disclaimer-link'>Privacy Act</a>. This includes, for example, the age, marital status or address of an identifiable individual.</li>
    <li>The Platform may not be used to upload or save Protected B information or anything above. Protected B information is information that, if compromised, could cause serious injury to an individual, an organization or government. Examples include: combinations of individual data elements (of Protected A information), contract negotiations, risk assessments, government decision-making documents, trade secrets, business-confidential records from producers, industry partners, individual’s finances.</li>
    <li>Unauthorized use of the Platform, such as, but not limited to, using it for personal storage or storing either personal information as defined under the <a href='http://laws-lois.justice.gc.ca/eng/acts/P-21/' class='disclaimer-link'>Privacy Act</a> or information above Protected A, may result in the user’s access being revoked immediately.</li>
</ul>
<h2>Monitoring</h2>
<ul>
<li>       AAFC reserves the right to monitor network activity on the Platform. That monitoring will be carried out for various purposes, such as assessing system or network performance, protecting government resources, and ensuring compliance with the intent of the Platform. All blocking and monitoring will be done in compliance with the <a href='http://laws-lois.justice.gc.ca/eng/acts/P-21/' class='disclaimer-link'>Privacy Act</a> and the <a href='http://laws-lois.justice.gc.ca/eng/Const/page-15.html#h-39' class='disclaimer-link'>Canadian Charter of Rights and Freedoms</a>.</li>
<li>       AAFC employs software programs to monitor network traffic and to identify unauthorized attempts to upload or change information, or otherwise cause damage. This software receives and records the IP address of the computer/device that has contacted our website, the date and time of the visit and the pages visited. No attempt to link these addresses with the identity of individuals visiting the Platform will be made unless an attempt to damage the Platform or criminal activity has been made. This information is collected pursuant to section 161 of the <a href='https://laws-lois.justice.gc.ca/eng/acts/f-11/' class='disclaimer-link'>Financial Administration Act</a>. The information may be shared with appropriate law enforcement authorities if suspected criminal activities are detected. </li>
</ul>
<h2>Access to information</h2>
<ul>
<li>       All information transmitted and stored on Government of Canada networks and devices, whether professional or personal in nature, may be accessible under the <a href='http://laws-lois.justice.gc.ca/eng/acts/A-1/' class='disclaimer-link'>Access to Information Act</a> and the <a href='http://laws-lois.justice.gc.ca/eng/acts/P-21/' class='disclaimer-link'>Privacy Act</a>, subject to exclusions and exemptions under these Acts.</li>
</ul>
<br>
<hr>
By selecting Yes below, you acknowledge our AAFC Disclaimer
</div>
<div lang="fr" class="frconst">

<h1 tabindex="1">Avis de non-responsabilité d’AAC</h1>

Conditions d'utilisation de la plateforme de stockage de données en nuage des Laboratoires vivants<br>
L'accès à la plateforme de stockage de données en nuage des Laboratoires vivants (la Plateforme) est réservé aux utilisateurs autorisés. Bien que la Plateforme soit gérée et surveillée par Agriculture et Agroalimentaire Canada (AAC), les données/informations qu'elle contient proviennent de sources internes et externes du gouvernement du Canada.<br>
<br>
<h2>Utilisation</h2>
<ul>
<li>       Vous êtes un utilisateur autorisé de la Plateforme</li>
<li>       La Plateforme peut seulement être utilisée pour partager des données/informations (qualité des sols, hydrogrammes, émissions de gaz à effet de serre, biodiversité, etc.) qui se rapportent aux initiatives approuvées des laboratoires vivants et pour consulter les données/informations soumises par d'autres utilisateurs.</li>
<li>       AAC ne donne aucune garantie, expresse ou implicite, et n'accepte aucune responsabilité légale ou responsabilité sur l'exactitude, l'exhaustivité ou l'utilité de toute information sur la Plateforme.</li>
<li>       La Plateforme ne peut contenir que des informations d'un niveau de sécurité maximum Protégé A et vous êtes responsable de vous assurer qu'aucune donnée supérieure à ce niveau n'est téléchargée ou enregistrée sur la Plateforme. Les informations Protégé A sont des informations qui, si elles sont compromises, pourraient causer un préjudice à un individu, une organisation ou un gouvernement. Exemples : contrats, appels d'offres, propositions non sollicitées et plaidoiries. </li>
<li>       La Plateforme ne peut pas être utilisée pour télécharger ou sauvegarder des informations personnelles telles que définies dans la <a href='https://laws-lois.justice.gc.ca/fra/lois/P-21/' class='disclaimer-link'>Loi sur la protection des renseignements personnels</a>. Cela inclut, par exemple, l'âge, l'état civil ou l'adresse d'une personne identifiable.</li>
<li>       La plateforme ne peut pas être utilisée pour télécharger ou sauvegarder des informations protégées B ou toute autre information ci-dessus. Les informations Protégé B sont des informations qui, si elles sont compromises, pourraient causer un préjudice grave à un individu, une organisation ou un gouvernement. Exemples : combinaisons d'éléments de données individuels (d'informations Protégé A), négociations de contrats, évaluations des risques, documents de décision du gouvernement, secrets commerciaux, documents commerciaux confidentiels des producteurs, partenaires industriels, aspects financiers d'un particulier.</li>
<li>       L'utilisation non autorisée de la Plateforme, telle que, mais non limitée à, l'utilisation pour le stockage personnel ou le stockage soit d'informations personnelles telles que définies dans la <a href='https://laws-lois.justice.gc.ca/fra/lois/P-21/' class='disclaimer-link'>Loi sur la protection des renseignements personnels</a>, soit d'informations au-dessus du niveau Protégé A, peut entraîner la révocation immédiate de votre accès.</li>
</ul>
<h2>Surveillance</h2>
<ul>
<li>       AAC se réserve le droit de surveiller l'activité du réseau sur la Plateforme. Cette surveillance sera effectuée à des fins diverses, telles que l'évaluation de la performance du système ou du réseau, la protection des ressources gouvernementales et la garantie de la conformité avec l'intention de la Plateforme. Tout blocage et toute surveillance seront effectués conformément à la <a href='https://laws-lois.justice.gc.ca/fra/lois/P-21/' class='disclaimer-link'>Loi sur la protection des renseignements personnels</a> et à la <a href='https://laws-lois.justice.gc.ca/fra/Const/page-15.html' class='disclaimer-link'>Charte canadienne des droits et libertés</a>.</li>
<li>       AAC utilise des logiciels pour surveiller le trafic sur le réseau et pour identifier les tentatives non autorisées de téléchargement ou de modification des informations, ou pouvant causer d'autres dommages. Ce logiciel reçoit et enregistre l'adresse IP de l'ordinateur ou de l'appareil qui a contacté notre site web, la date et l'heure de la visite et les pages consultées. Aucune tentative de relier ces adresses à l'identité des personnes visitant la Plateforme ne sera faite à moins qu'une tentative de dommage à la Plateforme ou activité criminelle n'ait été faite. Ces informations sont collectées conformément à l'article 161 de la <a href='https://laws-lois.justice.gc.ca/fra/lois/f-11/' class='disclaimer-link'>Loi sur la gestion des finances publiques</a>. Les informations peuvent être partagées avec les autorités policières appropriées si des activités criminelles suspectes sont détectées.</li>
</ul>
<h2>Accès à l’information</h2>
<ul>
<li>       Toutes les informations transmises et stockées sur les réseaux et dispositifs du gouvernement du Canada, qu'elles soient de nature professionnelle ou personnelle, peuvent être accessibles en vertu de la <a href='https://laws-lois.justice.gc.ca/fra/lois/A-1/' class='disclaimer-link'>Loi sur l'accès à l'information</a> et de la <a href='https://laws-lois.justice.gc.ca/fra/lois/P-21/' class='disclaimer-link'>Loi sur la protection des renseignements personnels</a>, sous réserve des exclusions et des exemptions prévues par ces lois.</li>
</ul>
En sélectionnant oui ci-dessous, vous reconnaissez l’avis de non-responsabilité d'AAC :<br>
 
</div>
`
const cancel_msg_en = "Reload the page and accept the terms of use if you wish to proceed."
const cancel_msg_fr = "Recharger la page et accepter les conditions d'utilisation si vous souhaitez poursuivre."
const url = window.location.href

//define as global so login.js can see it
var Login_Button_Text = "Log in";
var Logging_in_Button_Text = "Logging in ...";
var langfor = document.getElementsByTagName('html')[0].getAttribute('lang');

if (langfor === 'fr') {
  Login_Button_Text = "Connexion";
  Logging_in_Button_Text = "Connexion ...";
}
else{
  Login_Button_Text = "Log in";
  Logging_in_Button_Text = "Logging in ...";
}


// Hide login splash
function hideSplash() {
    
    $("main").hide();
    $("header").append("<p class='info'><a href='.'>" + cancel_msg_en + "</a></p>")
    $("header").append("<p class='info'><a href='.'>" + cancel_msg_fr + "</a></p>")
}

// Conditions to skip the TOS
//      1. Logout (clear=)
//      2. Failed login (user=)
function showTOS() {
    if (url.indexOf("clear=") != -1 || url.indexOf("user=") != -1 || url.indexOf("totp") != -1 || url.indexOf("setupchallenge") != -1) {
        return false; 
    }
    return true;
}

//Cookie API to help pass Language Choice to core
function createCookie(name, value, days) {
    var expires;

    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        expires = "; expires=" + date.toGMTString();
    } else {
        expires = "";
    }
    document.cookie = encodeURIComponent(name) + "=" + encodeURIComponent(value) + expires + "; path=/";
}

function readCookie(name) {
    var nameEQ = encodeURIComponent(name) + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) === ' ')
            c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) === 0)
            return decodeURIComponent(c.substring(nameEQ.length, c.length));
    }
    return null;
}

function eraseCookie(name) {
    createCookie(name, "", -1);
}

/* Using JQuery dialogue box to implement https://api.jqueryui.com/dialog/ */
function createDialogBoxFR(){
  $("#body-login").append( "<div id='disclaimer'>" +terms+"</div>" );
  $('.enconst').hide();
  $("#disclaimer").dialog({
      dialogClass: "custom-dialogue",
      draggable: false,
      resizable: false,
      fluid: true,
      buttons: [
          {
            text: "Oui",
            lang:"fr",
            click: function() {
              $( this ).dialog( "close" );
              $('.wrapper').show();
              $('.grouptop input').attr("placeholder", "Utilisateur ou email");
              $('.grouptop input').attr("aria-label", "Utilisateu ou email");
              $('.groupbottom input').attr("placeholder", "Mot de passe");
              $('.groupbottom input').attr("aria-label", "Mot de passe");
              $('#submit-wrapper input').val("Connexion");
              document.documentElement.setAttribute('lang','test');
              Login_Button_Text = "Connexion";
              Logging_in_Button_Text = "Connexion...";   
              createCookie("LanguageChosen", "fr");
            }
          },
         
          {
              text: "Non",
              lang:"fr",
              class: "no-button",
              click: function() {
                $('.wrapper').show();
                hideSplash();
                $( this ).dialog( "close" );
              }
          }
      ],
      open: function(event, ui) { 
        $('#disclaimer h1')[0].focus(); 
      }
  });
  $('.wrapper').hide();
  $(".ui-dialog-titlebar").hide();

}
function createDialogBoxEN(){
$("#body-login").append( "<div id='disclaimer'>" +terms+"</div>" );
$('.frconst').hide();
$("#disclaimer").dialog({
    dialogClass: "custom-dialogue",
    draggable: false,
    resizable: false,
    fluid: true,
    buttons: [
        {
            text: "Yes",
            lang:"en",
            click: function() {
              $( this ).dialog( "close" );
              $('.wrapper').show();
              $('#submit-wrapper input').val("Log in");
              Login_Button_Text = "Log in";
              Logging_in_Button_Text = "Logging in ..."
              document.documentElement.setAttribute('lang','test');
              createCookie("LanguageChosen", "en");	
            }
          },
        {
            text: "No",
            lang:"en",
            class: "no-button",
            click: function() {
              $('.wrapper').show();
              hideSplash();
              $( this ).dialog( "close" );
            }
        }
    ],
    open: function(event, ui) { 
      $('#disclaimer h1')[0].focus(); 
    }
});
$('.wrapper').hide();
$(".ui-dialog-titlebar").hide();

}
function createLang(){
$("#body-login").append( "<div id='disclaimers'>"+"</div>" );
$("#disclaimers").dialog({
    dialogClass: "custom-dialogue-lang",
    draggable: false,
    resizable: false,
    fluid: true,
    buttons: [
        {
            text: "English",
            lang:"en",
            click: function() {
              $(this).dialog("close");
              $("html").attr("lang","en");
              createDialogBoxEN();  
            }
          },
          {
            text: "Français",
            lang:"fr",
            click: function() {
              $(this).dialog("close");
              $("html").attr("lang","fr");
              createDialogBoxFR();  
            }
          }
    ]
});
$('.wrapper').hide();
$("#disclaimers").hide();
$(".ui-dialog-titlebar").hide();
}

// Disclaimer for pre-login
if (url.indexOf("login") != -1 && showTOS()) { 

  createLang();
}
// Hide login form if user is logging out, force them to reload page and re-accept the tos
if (url.indexOf("clear") != -1) {
    hideSplash();
}
// on window resize run function
$(window).resize(function () {
  fluidDialog();
});

// catch dialog if opened within a viewport smaller than the dialog width
$(document).on("dialogopen", ".ui-dialog", function (event, ui) {
  fluidDialog();
});

function fluidDialog() {
  var $visible = $(".ui-dialog:visible");
  // each open dialog
  $visible.each(function () {
      var $this = $(this);
      var dialog = $this.find(".ui-dialog-content").data("ui-dialog");
      // if fluid option == true
      if (dialog.options.fluid) {
          var wWidth = $(window).width();
          // check window width against dialog width
          if (wWidth < (parseInt(dialog.options.maxWidth) + 50))  {
              // keep dialog from filling entire screen
              $this.css("max-width", "90%");
          } else {
              // fix maxWidth bug
              $this.css("max-width", dialog.options.maxWidth + "px");
          }
          //reposition dialog
          dialog.option("position", dialog.options.position);
      }
  });

}
