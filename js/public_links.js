function hideSharing() {
	setInterval( function() {
		if(document.getElementsByClassName("sharing-link-list").length > 0){
			document.getElementsByClassName("sharing-link-list").forEach(
				function(sharingLink) {
					if(sharingLink.style.display.length == 0){
						sharingLink.style.display = 'none';
					}
				}
			)
		} 
	}, 500 );
}

//Go to settings page, find groups-groups node, iterate over children. Look for "admin". Hide sharing if you're allowed
function getRole() {
	var isAdmin = false;
	$(function () {
		$.ajax({
			url: OC.webroot + "/index.php/settings/user",
			dataType: "html",
			success: function (data) {
				if($(data).find('#groups-groups')[0] != null) {
					$(data).find('#groups-groups')[0].children.forEach(
						function(eachChild) {
							if(eachChild.innerText == "admin" || eachChild.innerText == "Living Lab Admins")
								isAdmin = true;
						}
					);
				}
			},
			complete: function(data) {
				if (!isAdmin) { hideSharing(); }
			}
		});
	});
}


// Look for sharing and comments tab every second. Add label for screen reader
setInterval( function() {
	//Add NVDA for share tab entry field reader
	if($('#tab-sharing .sharing-input input').length > 0){
		if($('#tab-sharing .sharing-input #tabSharingDivInput').length == 0) {
			//add label for screen reader
			$('#tab-sharing .sharing-input input').wrap('<div id="tabSharingDivInput"/>').parent().append($('<label class="hidden-visually">Name, federated cloud ID or email address</label>'));
		}
	}

	if($('#tab-sharing #collection-list input').length > 0){
		if($('#tab-sharing #collection-list #tabSharingAddToProject').length == 0) {
			//add label for screen reader
			$('#tab-sharing #collection-list input').wrap('<div id="tabSharingAddToProject"/>').parent().append($('<label class="hidden-visually">Add to a project</label>'));
		}
	}

	//Add NVDA for comments reader
	if($('#commentsTabView input').length > 0){
		if($('#commentsTabView #tabCommentsDivInput').length == 0) {
			//add label for screen reader
			$('#commentsTabView input').wrap('<div id="tabCommentsDivInput"/>').parent().append($('<label class="hidden-visually">New comment</label>'));
		}
	}


	if($('#commentsTabView .newCommentForm input .aria-label').length == 0){
		$('#commentsTabView .newCommentForm input').attr('aria-label', 'Post comment');
	}

	//WCAG 1.3.1: H65: Add title to + button
	if($('#body-user #content #app-content-files #controls .actions a').length > 0){
		if(!($('#body-user #content #app-content-files #controls .actions a')[0].getAttribute("aria-label")) ||
			$('#body-user #content #app-content-files #controls .actions a')[0].getAttribute("aria-label") == ""){
				$('#body-user #content #app-content-files #controls .actions a')[0].setAttribute("aria-label", "Create a new Folder , File or Text Document"); 
		//4.1.2 Add click handler for download options
		$('#body-user #content #app-content-files #controls .actions a')[0].onclick = function () {
			toggleExpandedForButton('#body-user #content #app-content-files #controls .actions a');
		}
		//Initial state of #header actions toggle
		toggleExpandedForButton('#body-user #content #app-content-files #controls .actions a');
			}
	}


	//WCAG 3.3.2
	if($('#app-content-files #controls .button').length > 0){
		if(!($('#app-content-files #controls .button')[0].hasAttribute("role"))){
			$('#app-content-files #controls .button')[0].setAttribute('role', 'button');
		}
	}
 	//WCAG 1.3.1 add text to Password label
	if($('#body-login .groupbottom label').length > 0){
			if(($('#body-login .groupbottom label').text() === "")){
					$('#body-login .groupbottom label').text("Password");
			}
	}

	//WCAG 3.2.4: Public download: footer link
	if($('#body-public footer p a').length > 0){
			$('#body-public footer p a').remove();
	}
	
	//WCAG 1.3.1 Public Share. Right header., add label to remote address on public shares
	if($('#external-share-menu-item .save-form label').length == 0){
		if($('#external-share-menu-item .save-form').length > 0) {
			$('<label class=\"hidden-visually\" for=\"remote_address\">Enter your Nextcloud email to add file to your Nextcloud account\"</label>').insertBefore(
				$('#external-share-menu-item .save-form #remote_address'));
		}
	}

	//WCAG 1.3.1 Public Share. Right header. Download options
	if($('#header-secondary-action #header-actions-toggle').length > 0){
		if(!$('#header-secondary-action #header-actions-toggle')[0].hasAttribute('aria-label')) {
			$('#header-secondary-action #header-actions-toggle')[0].setAttribute('aria-label', 'Download options');

			//4.1.2 Add click handler for download options
			$('#header-secondary-action #header-actions-toggle')[0].onclick = function () {
				toggleExpandedForButton('#header-secondary-action #header-actions-toggle');
			}
			//Initial state of #header actions toggle
			toggleExpandedForButton('#header-secondary-action #header-actions-toggle');
		}
	}

	//WCAG 4.1.2: Expand collapse for Action Buttons
	if($('a.actions-selected').length > 0){
		//iterate over each. Make sure there's expand/collapse
		$('a.actions-selected').each( function(index, anchor) {
			if(!anchor.hasAttribute('aria-expanded')) {
				//add click handler
				anchor.onclick = function () {
					toggleExpandedForDomItem(anchor);
				}
				//Initial state
				toggleExpandedForDomItem(anchor);
			};
		})
	}

	
	//WCAG 4.1.2: Expand collapse for Action Buttons
	if($('#createNew').length > 0){
		//iterate over each. Make sure there's expand/collapse
		$('#createNew').each( function(index, anchor) {
			if(!anchor.hasAttribute('aria-expanded')) {
				//add click handler
				anchor.onclick = function () {
					toggleExpandedForDomItem(anchor);
				}
				//Initial state
				toggleExpandedForDomItem(anchor);
			};
		})
	}

	//WCAG 4.1.2 Expand/collapse selector
	function toggleExpandedForButton(selector) {
		toggleExpandedForDomItem($(selector)[0]);
	}
	//WCAG 4.1.2 Expand/collapse domItem
	function toggleExpandedForDomItem(domItem) {
		if((domItem.getAttribute('aria-expanded') == null) ||
			(domItem.getAttribute('aria-expanded') == "true")){
				domItem.setAttribute('aria-expanded', 'false');
		} else {
			domItem.setAttribute('aria-expanded', 'true');
		}
	}


	//Filelist table too
	if($('a.action action-menu permanent').length > 0){
		//iterate over each. Make sure there's expand/collapse
		$('a.action action-menu permanent').each( function(index, anchor) {
			if(!anchor.hasAttribute('aria-expanded')) {
				//add click handler
				anchor.onclick = function () {
					toggleExpandedForDomItem(anchor);
				}
				//Initial state
				toggleExpandedForDomItem(anchor);
			};
		})
	}

	


	
		//WCAG 4.1.2 (...) needs role of button
		if($('#app-content-files .action.action-menu.permanent').length > 0){
			$('#app-content-files .action.action-menu.permanent').each(
				function(index, eachAnchor) {
					if(!eachAnchor.hasAttribute("role")){
						eachAnchor.setAttribute('role', 'button');
					}
				}
			)
			
		}

	//WCAG 4.1.2 Notes editor needs aria-label
	if($('#app-content #rich-workspace #editor-wrapper .editor__content .ProseMirror').length > 0){
		if(!$('#app-content #rich-workspace #editor-wrapper .editor__content .ProseMirror')[0].hasAttribute("aria-label")){
			$('#app-content #rich-workspace #editor-wrapper .editor__content .ProseMirror')[0].setAttribute("aria-label", "Type in comments in this text editor");
		}
	}

	//WCAG 4.1.2 PDF Frame title
	if($('#pdframe').length > 0){
		if($('#pdframe').attr('title') == null){
			$('#pdframe').attr('title', 'pdf viewer');
		}
	}

	//WCAG 2.4.4: Link Purpose (Public share Download button)
	if($('#header #h1-primary-action').length == 0){
		if($('#header-primary-action').length > 0) {
			var filename244 = $('#header #nextcloud .header-appname').text().trim();
			$('<h1 id=\"h1-primary-action\" class=\"hidden-visually\">' + filename244 + '</h1>').insertBefore($('#header-primary-action'));
			$('#header-primary-action a').attr('aria-labelledby', "download_h1-primary-action h1-primary-action");
			$('#header-primary-action a').attr('id', "download_h1-primary-action");

			//2.4.4: alt text public preview image: Download [filename]
			$('.publicpreview').attr('alt', 'Download ' + filename244);
		}
	}

	//WCAG 2.4.4 Side Panel, Share overview "...", Purpose of link
	if($('#body-user #content header .app-sidebar-header__menu').length > 0){
		if($('#body-user #content header .app-sidebar-header__menu a span.hidden-visually').length == 0){
			$('#body-user #content header .app-sidebar-header__menu a').append(
				$('<span class="hidden-visually">Share Actions</span>')
			);
		}
	}
	

	//WCAG 4.1.2 Page3: add aria-expanded to share on left
	if($('button.collapse.app-navigation-noclose').length > 0){
		if($('button.collapse.app-navigation-noclose')[1].onclick == null) {
			$('button.collapse.app-navigation-noclose')[1].onclick = function () {
				hideShowShareOnLeft();
			}
			//trigger first time, so first click will enable aria-expanded
			//hideShowShareOnLeft();
		}
	}

	//WCAG 4.1.2 Page3: add aria-expanded to share on left, remove if there
	function hideShowShareOnLeft() {
		if(//$("#app-navigation li.nav-shareoverview").hasClass( "open" ) ||  
		 ($('button.collapse.app-navigation-noclose')[1].getAttribute('aria-expanded') == null) ||
			($('button.collapse.app-navigation-noclose')[1].getAttribute('aria-expanded') == "true")){
			$('button.collapse.app-navigation-noclose')[1].setAttribute('aria-expanded', 'false');
			//handle lli parent at same time
			$('#app-navigation li.nav-shareoverview')[0].setAttribute('aria-expanded', 'false');
		} else {
			$('button.collapse.app-navigation-noclose')[1].setAttribute('aria-expanded', 'true');
			//handle lli parent at same time
			$('#app-navigation li.nav-shareoverview')[0].setAttribute('aria-expanded', 'true');
		}
	}
}, 1000 );

//get the users role (admin or not), then hideSharing, which will depend on role
window.onload = getRole();



