/*
 * JS files that adds all necessary HTML elements to be WCAG compliant. This will typically pertain
 * to keyboard accessability and screen readers rather than the look of the site which can be changed
 * through the CSS and SCSS files.
 */

/* WCAG 1.3.1: All files */
/* Added span to toggle grid button so that screen readers can identify what button does */
if ($("#app-content #view-toggle").length === 1) {
    $("#app-content #view-toggle").append('<span class="hidden-visually">Toggle grid view</span>');
}

/* WCAG 3.2.3: Left nav panel */
/* Added title to show users that the link opens in a new window */
if ($("#app-settings-content em a").length === 1) {
    $("#app-settings-content em a").attr('title', 'Opens in new window');
}

/* WCAG 3.3.2: Left nav panel */
/* Added title to favorites and shares dropdown button to give screen readers more clarity*/
if ($("#app-navigation li.nav-favorites button").length === 1) {
    $("#app-navigation li.nav-favorites button").attr('title', "Favourites list");
}
if ($("#app-navigation li.nav-shareoverview button").length === 1) {
    $("#app-navigation li.nav-shareoverview button").attr('title', "Shares options");
}
if ($("#app-navigation li.nav-shareoverview").hasClass("open")) {
    $("#app-navigation li.nav-shareoverview").attr('aria-expanded', "true");
    $("button.collapse.app-navigation-noclose").attr('aria-expanded', "true");
} else {
    $("#app-navigation li.nav-shareoverview").attr('aria-expanded', "false");
    $("button.collapse.app-navigation-noclose").attr('aria-expanded', "false");
}

/* WCAG 4.1.1 */
$("#header").removeAttr('role');
if ($("#apps ul li a svg").length > 0) {
    $("#apps ul li a svg").removeAttr("alt");
}

var langfor = document.getElementsByTagName('html')[0].getAttribute('lang');

if (langfor === 'fr') {
    /* WCAG 4.1.1, 4.1.2 */
    if ($("#filestable").length > 0) {
        $('<div>Dossier(s) sélectionné(s)</div>').insertBefore($('#filestable'))
    }
} else if (langfor === 'en') {
    /* WCAG 4.1.1, 4.1.2 */
    if ($("#filestable").length > 0) {
        $('<div>Select File(s)</div>').insertBefore($('#filestable'))
    }
} else {
    /* WCAG 4.1.1, 4.1.2 */
    if ($("#filestable").length > 0) {
        $('<div>Select File(s)</div>').insertBefore($('#filestable'))
    }

}


/* will work if timed properly

if($(".newCommentForm").length > 0) {
    $('<div>New comment …</div>').insertBefore($('.newCommentForm div.message'));
}
if($("#tab-sharing .sharing-input").length > 0) {
    $("<div>" , t("Name, federated cloud ID or email address …") + "</div>").insertBefore($('#tab-sharing .sharing-input'));
}

//add aria-expanded true/false (need delay until they appear)
$('#fileList .filename .action-share').on("click", function() {
    $('#fileList .filename .action-share').attr("aria-expanded", "true");
});
*/

if ($('.notification .notification-subject .text').length > 0) {
    $('.notification .notification-subject .text').attr('style', 'line-break: inherit;');
}
//WCAG H67
if ($('.notification img.notification-icon').length > 0) {
    //alt = ""
    $('.notification img.notification-icon').attr('alt', "");
}